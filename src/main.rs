pub mod split;

use std::fs::File;

fn main() {
    let mut f = File::open("./EBOOT.PBP").unwrap();
    split::split_pbp(&mut f).unwrap();
}
