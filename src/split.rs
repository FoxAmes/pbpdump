use std::{
    fs::File,
    io::{Read, Seek, SeekFrom, Write},
};

use libpbpr::header::PBPHeader;

/// Split a PBP file into its components.
/// Writes to the current directory.
pub fn split_pbp(f: &mut File) -> Result<(), String> {
    // Parse header
    let header = match PBPHeader::from_file(f) {
        Ok(h) => h,
        Err(e) => return Err(format!("Unable to parse header: {}", e)),
    };

    // Write metadata
    write_sub_file(
        f,
        "param",
        "sfo",
        header.metadata_offset,
        header.thumbnail_offset - header.metadata_offset,
    )?;
    // Write thumbnail
    write_sub_file(
        f,
        "thumbnail",
        "png",
        header.thumbnail_offset,
        header.code_offsets[0] - header.thumbnail_offset,
    )?;
    // Dump code
    write_sub_file(
        f,
        "code",
        "bin",
        header.code_offsets[0],
        header.data_offset - header.code_offsets[0],
    )?;
    // Write data
    let f_len = f.seek(SeekFrom::End(0)).expect("Unable to get file length") as u32;
    write_sub_file(
        f,
        "data",
        "psar",
        header.data_offset,
        f_len - header.data_offset,
    )?;
    Ok(())
}

/// Creates a file from a section of a parent file. Does not restore cursor.
///
/// # Parameters
///
/// `s`: source `File` reference.
///
/// `n`: base name for the file.
///
/// `e`: file extension to use.
///
/// `o`: offset (in bytes) in source file to start reading from.
///
/// `l`: length (in bytes) to read from source file.
fn write_sub_file(s: &mut File, n: &str, e: &str, o: u32, l: u32) -> Result<(), String> {
    // Attempt to seek to offset in file
    match s.seek(SeekFrom::Start(o as u64)) {
        Ok(_) => {}
        Err(e) => return Err(format!("Unable to skip to {} offset: {}", n, e)),
    };
    // Attempt to create output file
    let mut out_f = match File::create(format!("./{}.{}", n, e)) {
        Ok(f) => f,
        Err(err) => return Err(format!("Unable to create {} file: {}", n, err)),
    };
    // Write source content to output file
    let mut content: Vec<u8> = vec![0; l as usize];
    match s.read(&mut content) {
        Ok(_) => {}
        Err(err) => return Err(format!("Unable to read source file: {}", err)),
    };
    match out_f.write_all(&content) {
        Ok(_) => {}
        Err(err) => return Err(format!("Unable to write to {} file: {}", n, err)),
    };
    Ok(())
}
